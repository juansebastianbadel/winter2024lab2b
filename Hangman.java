import java.util.Scanner;
public class Hangman{
    public static void main(String[] args){
        Scanner reader = new Scanner(System.in);
        System.out.println("Provide the word to guess");
        String target = reader.nextLine();

        runGame(target);
    }


    public static int isLetterInWord(String word, char c){
       for(int i = 0; i < word.length(); i++){
        if(toUpperCase(c) == toUpperCase(word.charAt(i)))
            return i;
		//return position if current letter is the same as c
       }
       return -1;
	   //if no letters are the same as c,  return -1;
    }
   
   
    public static char toUpperCase(char c){
        return Character.toUpperCase(c);
    }
   
   
    public static void printWork(String word, boolean[] lettersBoolean){

        String maybe = "";
		for(int i = 0; i < lettersBoolean.length; i++){
			if (lettersBoolean[i]){
				maybe += word.charAt(i);
			}else {
            maybe += "_";
			}
		}
        System.out.println("Your result is " + maybe);
    }

    public static void runGame(String word){
		boolean[] lettersBoolean = {false, false, false, false};
        int misses = 0;
		
        Scanner reader = new Scanner(System.in);
        while(misses < 6 && (!lettersBoolean[0] || !lettersBoolean[1] || !lettersBoolean[2] || !lettersBoolean[3])){
            System.out.println("Please guess a letter");
            char guess = reader.nextLine().charAt(0);
            int position = isLetterInWord(word,guess);
				
				for(int i = 0; i < lettersBoolean.length; i++){
					if(position == i){
						lettersBoolean[i] = true;
					}
                }
                if(position == -1){
                    misses += 1; 
                }
                System.out.println("fails: " + misses); 
				printWork(word,lettersBoolean);				
                }
              }

}