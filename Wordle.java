import java.util.Random;
import java.util.Scanner;

public class Wordle{

    public static String generateWord(){
        Random rando = new Random();

        String[] possibleWords = new String[] {"write", "place", "knobs", "train", "crazy", 
        "blaze", "joker", "pluck", "flock", "picky", "inbox", "adore", "exact", "blame", "extra",
        "grown", "milky", "pubic", "mixer", "wreck", "water", "toxic", "bicep", "groin", "light",
        "focal", "focus", "train", "waste", "eight", "yacht", "adopt", "crate", "sedan", "paste",
        "paint", "faint", "whale", "quail", "image", "zebra", "crave", "dream", "steam", "stare",
        "flare", "great", "shale", "frail", "grail", "night", "sight", "fight", "chain", "break",
        "might", "flake", "poise", "noise", "voice", "abode", "chase", "cabin", "right", "trace",
        "satin", "haste", "urban", "eland"};

        int wordIndex = rando.nextInt(possibleWords.length);
        return possibleWords[wordIndex].toUpperCase();
    } 

    public static boolean letterInWord(String word, char x){
        
        boolean included = false;
        for(int i = 0; i < word.length(); i++){
         if(x == word.charAt(i))
            included = true;
        }
        return included;
    }

    public static boolean letterInSlot(String word, char x, int pos){
        boolean rightPosition = false;
        for(int i = 0; i < word.length(); i++){
            if(i == pos && x == word.charAt(i))
                rightPosition = true;
        }
        return rightPosition;
    }

    public static String[] guessWord(String answer, String guess){
        String[] colours = new String[]{"white", "white", "white", "white", "white"};
        for(int i = 0; i < answer.length(); i++){
            if(letterInWord(answer, guess.charAt(i)) == true){
                colours[i] = "yellow";
            }
            if(letterInSlot(answer, guess.charAt(i), i) == true){
                colours[i] = "green";
            }
        }
        return colours;
    }

    public static String presentResults(String word, String[] colours){

        final String GREEN = "\u001B[32m";
        final String YELLOW = "\u001B[33m";
        final String WHITE = "\u001B[0m";

        String coloredWord = ""; 

        for(int i = 0; i < colours.length; i++){
            switch(colours[i]){
                case "white":
                coloredWord += WHITE + word.charAt(i) + WHITE;
                break;
                case "yellow":
                coloredWord += YELLOW + word.charAt(i) + WHITE;
                break;
                case "green":
                coloredWord += GREEN + word.charAt(i) + WHITE;
                break;
            }
        }
        return coloredWord;
    }

    public static String readGuess(){
        Scanner reader = new Scanner(System.in);
        System.out.println("Please input a five letter word");
        String word = reader.nextLine();

        while(word.length() != 5){
            System.out.println("Word must be five letters long");
            word = reader.nextLine();
        }

        return word.toUpperCase();

    }

    public static void runGame(String target){
        String result = "";
        
        for(int i = 0; i < 6; i++){
            String word = readGuess();
            result = presentResults(word, guessWord(target, word));
            System.out.println(result + " -- " + (5 - i) + " attempts remaining");
            
            if(word.toUpperCase().equals(target)){
                System.out.println("Congrats you win! Come back tomorrow.");
                break;
            }
            if(i == 5 && !(word.toUpperCase().equals(target))){
                result = presentResults(word, guessWord(target, word));
                System.out.println(result);
                System.out.println("Sorry, the word was " + target + ". Try again tomorrow.");

            }
        }
    }
    
}
