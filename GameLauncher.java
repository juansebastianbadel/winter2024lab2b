import java.util.Scanner;

public class GameLauncher
{
	public static void main (String[] args)
	{
		System.out.println("Hello, what game do you want to play?");
		
		//initializing a Scanner to get user input (int) stored in the variable choice
		Scanner launchReader = new Scanner(System.in);
		System.out.println("Select 1 for Wordle or 2 for Hangman");
		int choice = launchReader.nextInt();
		
		//as long as the user doesn't select 1 or 2, they will be prompted to select 1 or 2
		while(choice != 1 && choice != 2){
			System.out.println("Please select 1 or 2");
			choice = launchReader.nextInt();
		}
		//if they select 1, the String word will store the result of the wordle function generateWord
		//then. the wordle function "runGame" will be ran		
		if(choice == 1){
			System.out.println("WORDLE");
			
			String word = Wordle.generateWord();
			Wordle.runGame(word);
		//else, which is necessarily 2 because of the while loop. a Scanner will take user input 
		//it will be used to run the Hangman function "runGame"
		}else{
			System.out.println("HANGMAN");
			
			Scanner hangmanReader = new Scanner(System.in);
			System.out.println("Provide the 4 letter word to guess");
			String target = hangmanReader.nextLine();
			Hangman.runGame(target);
		}
		
	}
}
